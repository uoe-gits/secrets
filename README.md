# secrets
secrets is a tool to retrieve secrets and store them in the filesystem.
The secrets are usually held in a asubversion repository, access to which
is controlled by kerberos.  Authentication using a suitable keytab allows
the files to be retrieved and then updated on the local filesystem.

