Summary: secrets - retrieve secrets for a computer
Name: secrets
Version: 2.0
Release: 1.geos
License: Copyright The University of Edinburgh 2014-2020
Group: LCFG/Utils
Source0: %{name}
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires: python3 python36-pysvn python36-distro

%description
secrets is a tool to retrieve secrets and store them in the filesystem.
The secrets are usually held in a asubversion repository, access to which
is controlled by kerberos.  Authentication using a suitable keytab allows
the files to be retrieved and then updated on the local filesystem.

All the configuration lives in an INI file in /etc/sysconfig

%prep

%setup -c ${name} -T
cp %{SOURCE0} .

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/sbin $RPM_BUILD_ROOT/etc/sysconfig
install -m 755 %{name} $RPM_BUILD_ROOT/usr/sbin/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(755,root,root)
/usr/sbin/%{name}

%changelog
* Mon Jun  1 2020 Magnus Hagdorn <mhagdorn@baltic10.geos.ed.ac.uk> - 2.0-1.geos
- new version of secrets using python3

* Thu Jun 19 2015 Magnus Hagdorn <Magnus.Hagdorn@ed.ac.uk> - 1.0.3.geos
- rebuild on SL7

* Thu Jun 19 2014 Shane Voss <Shane.Voss@ed.ac.uk> - 1.0.2.geos
- Move to /usr/sbin

* Mon Jun 16 2014 Shane Voss <Shane.Voss@ed.ac.uk> - 1.0.1.geos
- Initial build

